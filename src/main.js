import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Loading from "./components/Loading.vue";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import BaiduMap from 'vue-baidu-map'
// import './rem/res';
Vue.config.productionTip = false;
Vue.component('Loading', Loading);
// 引入echarts
import echarts from 'echarts';
//一般都要加个$加到vue的原型链上，方便引用
Vue.prototype.$echarts = echarts;
Vue.use(ElementUI);
Vue.use(BaiduMap, {
  /* Visit http://lbsyun.baidu.com/apiconsole/key for details about app key. */
  ak: 'x9lTMCv7hlY7dNF3PwMUKWEh126sse9z'
})
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
